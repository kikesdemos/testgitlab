package com.example.testgitlab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestgitlabApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestgitlabApplication.class, args);
	}

}
